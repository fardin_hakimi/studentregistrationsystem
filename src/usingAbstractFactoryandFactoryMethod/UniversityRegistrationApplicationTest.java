package usingAbstractFactoryandFactoryMethod;

public class UniversityRegistrationApplicationTest {
	
	public static void main(String args[]){
		University CS = new computerScience();
		
		CS.SubmitForm("fardin","ismail","CS","software engineering");
		CS.SubmitForm("jalil","fazil", "CS","software engineering");
		CS.SubmitForm("Aalem","Aalim","CS","software engineering");
		CS.SubmitForm("Farhad"," Omar","CS","database");
		CS.SubmitForm("parvez", "parviz", "CS", "network");
		CS.SubmitForm("mansour", "farhad", "CS", "database");
	}
}
