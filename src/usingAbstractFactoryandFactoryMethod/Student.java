package usingAbstractFactoryandFactoryMethod;

public abstract class Student{
	
	String name;
	String fatherName;
	String departmentName;
  //---------------------------//
	EmailAddress email;
	ID id;
	AttendenceSheet sheet;
	Department department;
	creditSheet subjects[];
	
	public abstract void addStudent();
	
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}
	public void setFname(String fName){
		this.fatherName=fName;
	}
	public String getFname(){
		return fatherName;
	}
	public void setDepartmentName(String name){
		this.departmentName=name;	
	}
	public String getDepartmentName(){
		return departmentName;
		
	}
}
