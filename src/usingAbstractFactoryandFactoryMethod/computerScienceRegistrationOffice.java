package usingAbstractFactoryandFactoryMethod;

import java.util.ArrayList;

public class computerScienceRegistrationOffice implements universityRegistrationOffice{
	private creditSheet[] subjects = null;
	private AttendenceSheet[] students = null;
	Department department=null;
	AttendenceSheet attendence=null;
	String departmentName;
	@Override
	public Department addDepartment(String departmentName){
		this.departmentName=departmentName;
		
		if(departmentName.equals("software engineering")){
			
		department=new SoftwareEngineering();
		}else if(departmentName.equals("database")){
			department= new Database();
		}else if(departmentName.equals("n")){
			department= new network();
		}
		return department;
	}
	@Override
	public EmailAddress createEmail(String name){
		EmailAddress address;
		return new CsEmailAddress(name);
		// TODO Auto-generated method stub
	}
	@Override
	public creditSheet[] produceCreditSheet(){
		
		System.out.println("Credit sheet:");
		if(departmentName.equals("software engineering")){
		 creditSheet subjects[]={new Islamic(), new Programming(),new webEngineering()};
		 this.subjects=subjects;
		 
		}else if(departmentName.equals("database")){
			 creditSheet subjects[]={new Islamic(),new webEngineering(),new databaseEngineering()};
			 this.subjects=subjects;
			 
	    }else if(departmentName.equals("network")){
			 creditSheet subjects[]={new Islamic(), new netWorkProgramming(), new networkProject()};
			 this.subjects=subjects;
			} else{
				System.out.println("The credit sheet for this department does not exist");
			}
		
	       return subjects;
	}

	@Override
	public ID generateId(){
		return new IdGenerator();
	}

	@Override
	public AttendenceSheet addtoAttendence(String name) {
		AttendenceSheet sheet=null;
		
		if(departmentName.equals("software engineering")){
			sheet= new softwareEngineeringSheet(name);
		}else if(departmentName.equals("database")){
			sheet= new databseSheet(name);
		}else if(departmentName.equals("network")){
		sheet= new networkSheet(name);
	}
		return sheet;
	}

}
