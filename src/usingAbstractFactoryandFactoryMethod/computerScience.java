package usingAbstractFactoryandFactoryMethod;


public class computerScience extends University{
	@Override
	public Student registerStudent(String name, String fatherName,String faculty, String department){
		Student student=null;
		// composite with university registration office
		universityRegistrationOffice office= new computerScienceRegistrationOffice();
		
		if(faculty.equals("CS")){
			student= new ComputerScienceStudent(office);
			student.setName(name);
			student.setFname(fatherName);
			student.setDepartmentName(department);
		}
		return student;
	}
	

}
