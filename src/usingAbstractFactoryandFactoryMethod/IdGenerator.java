package usingAbstractFactoryandFactoryMethod;
    
public class IdGenerator extends ID {
	private static long idCounter = 1;
IdGenerator(){
	System.out.println("ID: "+createID());
}
public synchronized String createID()
{
    return String.valueOf(idCounter++);
}
}
	

