package usingAbstractFactoryandFactoryMethod;

import java.util.ArrayList;

public interface universityRegistrationOffice{
	
	Department addDepartment(String department);
	
	creditSheet [] produceCreditSheet(); 
	
	EmailAddress createEmail(String name);
	
	AttendenceSheet addtoAttendence(String name);
	
	ID generateId();
}