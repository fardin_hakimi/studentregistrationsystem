package usingAbstractFactoryandFactoryMethod;

import java.util.Vector;

public class softwareEngineeringAttendenceSheet extends AttendenceSheet {
	softwareEngineeringAttendenceSheet(String name){
		super(name);
		AddName(name);
		System.out.println();
		System.out.println("Congratulations you have been added to the attendence sheet");
		System.out.println("Current members of software engineering department:");
	}
	public void AddName(String name){
		Vector<String>attendence= new Vector<String>();
		attendence.add(name);
		for(int i=0;i<attendence.size();i++){
			System.out.println(attendence.get(i));
		}
	}
}
