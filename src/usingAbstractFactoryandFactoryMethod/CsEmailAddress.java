package usingAbstractFactoryandFactoryMethod;

public class CsEmailAddress extends EmailAddress {

	public CsEmailAddress(String name) {
		System.out.println("Email Address: "+name+".cs@edu.com");
	}

}
