package usingAbstractFactoryandFactoryMethod;

public class ComputerScienceStudent extends Student{
	
	universityRegistrationOffice  office;
	
	public ComputerScienceStudent(universityRegistrationOffice office){
		this.office=office;
	}
	public void addStudent(){
		System.out.println("registering "+getName()+" in process......");
		System.out.println();
		System.out.println("Congratulations "+getName()+" you were succesfully registered into the system");
		System.out.println();
		System.out.println("here are your specifations");
		System.out.println("----------------------------");
		System.out.println("Name:"+getName());
		System.out.println("Father Name:"+getFname());
		id= office.generateId();
		email=office.createEmail(getName());
		department= office.addDepartment(getDepartmentName());
		sheet=office.addtoAttendence(getName());
		subjects=office.produceCreditSheet();
		System.out.println("**********************************");
		
		
	}

}
