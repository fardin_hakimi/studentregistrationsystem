package usingAbstractFactoryandFactoryMethod;

public abstract class University{
	
	public Student SubmitForm(String name,String fatherName,String faculty,String department){
		
		Student student=null;
		student=registerStudent(name,fatherName,faculty,department);
		
		student.addStudent();
		
		return student;
	}
	public abstract Student registerStudent(String name,String fatherName,String faculty,String department);
}
