package UsingSimpleFactory;

public class RegistrationFactory{
	
	      String name;

	public University registerStudent(String name, String fatherName,String faculty) {
		this.name=name;
		University registeredStudent = null;
		if(faculty.equals("Computer science")){
			registeredStudent= new ComputerScience();
		}
		return registeredStudent;
	
	}
	public String getName(){
		return name;
	}
	
}
